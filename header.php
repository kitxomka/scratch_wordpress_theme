<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="with=device-with">
        <title><?php bloginfo('name'); ?></title>
        <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?>>
    <div class="container">

        <!-- site-header -->
        <header class="site-header">
            <!-- hd-search -->
            <div class="hd-search">
                <?php get_search_form();?>
            </div>
            <h1 class="title">
                <a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>
            </h1>
            <h5 class="description">
                <span><?php bloginfo('description'); ?></span>
            </h5>

            <nav class="site-nav">
                <?php
                $args = array(
                    'theme_location' => 'primary'
                );
                ?>
                <?php wp_nav_menu( $args ); ?>
            </nav>
        </header>


<?php

function resources(){
    wp_enqueue_style('style', get_stylesheet_uri());
}
add_action('wp_enqueue_scripts', 'resources');

// Get top ancestor
function get_top_ancestor_id(){

    global $post;

    if($post->post_parent){
        $ancestors = array_reverse(get_post_ancestors($post->ID));
        return $ancestors[0];
    }
    return $post->ID;
}

//Does page have children?
function has_children(){

    global $post;

    $pages = get_pages('child_of=' . $post->ID);
    return count($pages);
}

//Customize excerpt word count length
function custom_excerpt_length(){
    return 25;
}
add_filter('excerpt_length', 'custom_excerpt_length');


//Theme setup
function setup(){

    //Nav Menus
    register_nav_menus(array(
        'primary' => __('Primary Menu'),
        'footer' => __('Footer Menu'),
    ));

    //Add feature image support
    add_theme_support('post-thumbnails');
    add_image_size('small-thumbnail', 180, 120, true);
    add_image_size('banner-image', 920, 500, array('left', 'top'));

    //Add post format support
    add_theme_support('post-formats', array('aside', 'gallery', 'link'));
}
add_action('after_setup_theme', 'setup');


//Add widget locations
function ourWidgetInit(){
//    register_sidebar(array(
//        'name' => 'Sidebar',
//        'id' => 'sidebar1',
//        'before_widget' => '<div class="widget-item">',
//        'after_widget' => '</div>',
//    ));
    register_sidebar(array(
        'name' => 'Footer Area 1',
        'id' => 'footer1',
        'before_widget' => '<div class="widget-item">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="my-special-class">',
        'after_title' => '</h4>'
    ));
    register_sidebar(array(
        'name' => 'Footer Area 2',
        'id' => 'footer2',
        'before_widget' => '<div class="widget-item">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="my-special-class">',
        'after_title' => '</h4>'
    ));
    register_sidebar(array(
        'name' => 'Footer Area 3',
        'id' => 'footer3',
        'before_widget' => '<div class="widget-item">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="my-special-class">',
        'after_title' => '</h4>'
    ));
    register_sidebar(array(
        'name' => 'Footer Area 4',
        'id' => 'footer4',
        'before_widget' => '<div class="widget-item">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="my-special-class">',
        'after_title' => '</h4>'
    ));
}
add_action('widgets_init', 'ourWidgetInit');


//Customize appearance option
function customize_register( $wp_customize ){
    $wp_customize->add_setting('link_color',array(
        'default' => '#006ec3',
        'transport' => 'refresh',
    ));
    $wp_customize->add_setting('btn_color',array(
        'default' => '#006ec3',
        'transport' => 'refresh',
    ));
    $wp_customize->add_setting('btn_hover_color',array(
        'default' => '#006ec3',
        'transport' => 'refresh',
    ));

    $wp_customize->add_section('standard_colors', array(
        'title' => __('Standard Colors', 'myTheme'),
        'priority' => 30,

    ));

    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'link_color_control', array(
        'label' => __('Link Color', 'myTheme'),
        'section' => 'standard_colors',
        'settings' => 'link_color',

    ) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'btn_color_control', array(
        'label' => __('Button Color', 'myTheme'),
        'section' => 'standard_colors',
        'settings' => 'btn_color',

    ) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control($wp_customize, 'btn_hover_color_control', array(
        'label' => __('Hover Button Color', 'myTheme'),
        'section' => 'standard_colors',
        'settings' => 'btn_hover_color',

    ) ) );
}
add_action('customize_register', 'customize_register');


// Output customize css
function customize_css(){ ?>
    <style type="text/css">
        a:link,
        a:visited{
            color: <?php echo get_theme_mod('link_color');?>;
        }
        .site-header nav ul li.current-menu-item a:link,
        .site-header nav ul li.current-menu-item a:visited,
        .site-header nav ul li.current-page-ancestor a:link,
        .site-header nav ul li.current-page-ancestor a:visited{
            background-color: <?php echo get_theme_mod('link_color');?> ;
        }
        .btn-a,
        .btn-a:link,
        .btn-a:visited,
        div.hd-search #searchsubmit{
            background-color: <?php echo get_theme_mod('btn_color');?> ;
        }
        .btn-a:hover,
        div.hd-search #searchsubmit:hover{
            background-color: <?php echo get_theme_mod('btn_hover_color');?> ;
        }

    </style>
<?php }
add_action('wp_head', 'customize_css');
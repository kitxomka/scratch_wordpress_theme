<?php

get_header(); ?>
<div class="site-content clearfix"> <!-- site content -->

  <?php if(have_posts()):
    while(have_posts()): the_post();
        the_content();
    endwhile;

    else:
        echo '<p>No content found</p>';

    endif; ?>

    <div class="home_columns clearfix">
        <div class="one-half">
            <h2>Latest Test</h2>
            <?php
            // test posts loop begins here
            $testPosts = new WP_Query('cat=4&posts_per_page=2');
            if($testPosts->have_posts()):
                while($testPosts->have_posts()): $testPosts->the_post();?>
                    <h4>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <span class="subtle-date"><?php the_time('n/j/Y'); ?></span>
                    </h4>
                    <?php the_excerpt(); ?>
                <?php endwhile;

            else:

            endif;
            wp_reset_postdata();?>

            <span class="horiz-center"><a href="<?php echo get_category_link(4); ?>" class="btn-a">View all Test Posts</a></span>

        </div>
        <div class="last-half">
            <h2>Latest New</h2>
            <?php  // new posts loop begins here
            $newPosts = new WP_Query('cat=6&posts_per_page=2');
            if($newPosts->have_posts()):
                while($newPosts->have_posts()): $newPosts->the_post();?>
                    <h4>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        <span class="subtle-date"><?php the_time('n/j/Y'); ?></span>
                    </h4>
                    <?php the_excerpt();?>
                <?php endwhile;
            else:

            endif;
            wp_reset_postdata(); ?>

            <span class="horiz-center"><a href="<?php echo get_category_link(6); ?>" class="btn-a">View all New Posts</a></span>

        </div>
    </div>

</div> <!-- /site content -->

<?php get_footer();
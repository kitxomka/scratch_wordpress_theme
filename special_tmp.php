<?php

/*
 * Template Name: Special Layout
 */

get_header();

if(have_posts()):
    while(have_posts()): the_post(); ?>
        <article class="post page">
            <h2><?php the_title(); ?></h2>

            <!-- info-box -->
            <div class="info-box">
                <h4>Disclaimer Title</h4>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                    nonummy nibh euismod tincidunt ut laoreet dolore magna aliquamerat volutpat.</p>

            </div>

            <span><?php the_content(); ?></span>
        </article>
    <?php endwhile;

else:
    echo '<p>No content found</p>';

endif;

get_footer();